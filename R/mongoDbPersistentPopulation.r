mongoDbPersistentPopulation = setRefClass("mongoDbPersistentPopulation",
  fields = list(
    connector = "mongo",#Obligatoire
    db = "character",
    population = "character",
    model="character",#Automatiquement fixé
    query="mongo.bson",#Obligatoire
    sort="mongo.bson",
    queryfields="mongo.bson",
    parents="list",#Non obligatoire
    elements="list",#Non obligatoire
    filter="list",#Non obligatoire
    exclude="list"#Non obligatoire
  ),
  methods = list(
    initFields = function(){
      .self$setModel()
      .self$setPopulation()
      .self$fetch()
    },
    getNameSpace = function(){
      return(paste(db,model,sep="."))
    },
    setPopulation = function(){
      population <<- "none"
    },
    setModel = function(){
      model <<- "none"
    },
    getCursor = function(){
      if(length(query)==0){
        query <<- mongo.bson.empty()
      }
      if(length(model)==0){
        .self$setModel()
      }
      return(mongo.find(connector,.self$getNameSpace(),query,sort,queryfields))
      
    },
    fetch = function(){
      cursor = .self$getCursor()
      while (mongo.cursor.next(cursor)){
        mpo = getRefClass(model)$new(connector = connector,content = mongo.bson.to.list(mongo.cursor.value(cursor)),parents=parents, db = db)
        mpo$initFields()
        elements[[mpo$getId()]] <<- mpo
      }
    },
    countElements = function(){
      return(mongo.count(connector,.self$getNameSpace(),query))
    },
    indexBy = function(keyName){
      if(keyName == "id"){
        return(elements)
      }else{
        elementsByIndex = list() 
        for(element in elements){
          elementsByIndex[[element$getContent(keyName)]] = element
        }
      }
      return(elementsByIndex)
    },
    filter = function(f,parameters){
      l = list()
      for(id in names(.self$elements)){
        element = .self$elements[id]
        if(call(f,list(element,parameters))){
          l[[element$id]]=element
        }
      }
      return(l)
    }
  )                                        
)