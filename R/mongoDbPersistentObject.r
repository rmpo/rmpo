mongoDbPersistentObject = setRefClass("mongoDbPersistentObject",
  fields = list(
    db = "character",
    connector = "mongo",
    population = "character",
    model = "character", 
    oid = "mongo.oid",
    content = "list",
    is.saved = "logical",
    parents = "list"
  ),
  methods = list(
    initFields = function(){
      .self$setPopulation()
      .self$setModel()
      if(length(content)==0){
        .self$fetch()
      }else{
        if (!is.null(content[["_id"]])){
          oid <<- content[["_id"]]
          is.saved <<- TRUE
        }else{
          oid <<- mongo.oid.create()
          content[["_id"]] <<- oid
          is.saved <<- FALSE
        }
      }
      .self$postInitialize()
    },
    setPopulation = function(){
      population <<- "none"
    },
    setModel = function(){
      model <<- "none"
    },
    postInitialize = function(){
      
    },
    getHost = function(){
      return(attributes(.self$connector)$host)
    },
    getId = function(){
      return(as.character(content[["_id"]]))
    },
    getNameSpace = function(){
      return(paste(db,model, sep="."))
    },
    getAttribute = function(keyName){
      return(content[[keyName]])
    },
    getForeignQuery = function(){
      buf <- mongo.bson.buffer.create()
      mongo.bson.buffer.append(buf, paste(model,"_id",sep=""),oid)
      return(mongo.bson.from.buffer(buf))
    },
    getSelfQuery = function(){
      buf <- mongo.bson.buffer.create()
      mongo.bson.buffer.append(buf,"_id",oid)
      return(mongo.bson.from.buffer(buf))
    },
    fetch = function(){
      content <<- mongo.bson.to.list(mongo.find.one(connector,.self$getNameSpace(), .self$getSelfQuery()))
    },
    update = function(key,value,persistence=TRUE){
      content[[key]] <<- value
      if(persistence){
        .self$save()
      }
    },
    save = function(){
      if(!is.saved) .self$insert()
      else mongo.update(connector,.self$getNameSpace(),.self$getSelfQuery(),content)
    },
    insert = function(){
      mongo.insert(connector,.self$getNameSpace(),content)
      return(oid)
    },
    getRelateds = function(relatedPopulation,sort = mongo.bson.empty(),queryfields = mongo.bson.empty()){
      l = list()
      l[[.self$model]] = .self
      mpos = getRefClass(relatedPopulation)$new(connector = connector,query = .self$getForeignQuery(),sort = mongo.bson.empty(),queryfields = mongo.bson.empty(),parents = l, db = db )
      mpos$initFields()
      return(mpos)
    },
    getParent = function(collection){
      if(class(.self$parent)==collection){
        return(.self$parent)
      }else{
        parentClass = eval(parse(text = collection))
        a = paste(collection,"_id",sep="")
        return(parentClass$new(connector = connector, oid = .self$getContent(a)))
      }
    },
    getContent = function(keyName){
      return(content[[keyName]])
    }
  )
)