mongoDbConnection = setRefClass("mongoDbConnection",
	fields = list(
    auth = "logical",
    host = "character",
	  username = "character",
    password = "character",
	  db = "character",
    connector = "mongo"
	),
  methods = list(
    initFields = function(){
      if(length(host)==0){
        host <<- "localhost"
      }
      if(length(auth)==0){
        auth <<- FALSE
      } 
      if(auth){
        reader_connector = mongo.create(host=host, name="",username="power_reader", password="tango", db=db)
        if(length(username)==0){
          username <<- readline("Username?")
        }else{
          username <<- username
        }
        if(length(password)==0){
          password <<- readline("Password?")
        }else{
          password <<- password
        }
      }else{
        reader_connector = mongo.create(host=host, name="",username="", password="", db="")
        username <<- ""
        password <<- ""
      }
      if(length(db)==0){
        if(mongo.is.connected(reader_connector)){
          dbs = mongo.get.databases(reader_connector)
          print("Dbs:")
          print(dbs)  
          inputdb = readline("Which database?")
          db <<- inputdb
        }
      }else{
        db <<- db
      }
      connector <<- mongo.create(host=host, name="",username=username, password=password, db=db)
      .self$postInitialize()
    },
    find = function(collection,query,sort,queryfields){
      if(collection=="nucleus"){
        population = "nuclei"
      }else{
        population = paste(collection,"s",sep="")
      }
      mpos = getRefClass(population)$new(connector = connector,query = query, sort = sort, queryfields = queryfields, db = db)
      mpos$initFields()
      return(mpos)
    },
    getParameters = function(){
      return(list(
        host = host,
        username = username,
        password = password,
        auth = auth,
        db = db
      ))
    },
	  getDatabases = function(){
		  return(mongo.get.databases(connector))
	  },
	  switchDb = function(dbname){
      db <<- dbname
	  }
  )
)
